# Executables (local)
DOCKER_COMPOSE = docker-compose
SYMFONY = symfony

# Misc
.DEFAULT_GOAL = help
.PHONY        = cs help initdb phpstan start stop

##—— General ——————————————————————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

##—— Local development ————————————————————————————————————————

initdb: ## Init database (drop, create, fixtures)
	@$(SYMFONY) console doctrine:database:drop --force \
	&& $(SYMFONY) console doctrine:database:create \
	&& $(SYMFONY) console doctrine:schema:update --force \
	&& $(SYMFONY) console doctrine:fixtures:load -n

start: ## Start dev env
	@$(DOCKER_COMPOSE) up --remove-orphans -d \
	&& $(SYMFONY) run -d yarn watch \
	&& $(SYMFONY) serve -d --no-tls

stop: ## Stop dev env
	@$(SYMFONY) server:stop \
	&& $(DOCKER_COMPOSE) stop

##—— Tools ————————————————————————————————————————————————————

cs: ## Fix code style
	@$(SYMFONY) php vendor/bin/php-cs-fixer fix

phpstan: ## Static analysis
	@$(SYMFONY) php vendor/bin/phpstan analyse
