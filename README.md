# Planning

Dead simple example of real-time planning

* Built with Symfony, Mercure and Stimulus.

Run `make start` to start the project (Symfony CLI & Docker required)
