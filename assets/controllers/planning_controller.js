import { Controller } from '@hotwired/stimulus'

const bgColor = 'hover'

export default class extends Controller {
  static targets = ['body']

  connect () {
    this.enableDragAndDrop()
    this.enableEventSource(this.element.dataset.subscription)
  }

  enableDragAndDrop () {
    const cells = this.element.getElementsByTagName('td')
    for (const cell of cells) {
      cell.addEventListener('dragenter', this.onDragEnter)
      cell.addEventListener('dragleave', this.onDragLeave)
      cell.addEventListener('dragover', this.onDragOver)
      cell.addEventListener('drop', event => {
        this.onDrop(event)
      })
    }
    this.bodyTarget.addEventListener('dragend', event => {
      if (event.target.matches('div')) {
        this.onDragEnd(event)
      }
    })
    this.bodyTarget.addEventListener('dragstart', event => {
      if (event.target.matches('div')) {
        this.onDragStart(event)
      }
    })
  }

  /**
   * @param {string} url
   */
  enableEventSource (url) {
    const eventSource = new EventSource(url)
    eventSource.onmessage = event => {
      const payload = JSON.parse(event.data)
      if (payload.operation === 'ADD') {
        this.occupationAdd({ ...payload.occupation })
      } else if (payload.operation === 'MOVE') {
        this.occupationRemove(payload.occupation.uuid)
        this.occupationAdd({ ...payload.occupation })
      } else {
        console.error('Unhandled event: ' + payload.operation)
      }
    }
  }

  /**
   * @param {string} day
   * @param {string} member
   * @param {string} title
   @param {string} uuid   */
  occupationAdd ({ day, member, title, uuid }) {
    const element = document.createElement('div')
    element.dataset.uuid = uuid
    element.draggable = true
    element.innerHTML = title
    const parent = document.querySelector('[data-day="' + day + '"][data-member="' + member + '"]')
    parent.appendChild(element)
  }

  /**
   * @param {string} uuid
   */
  occupationRemove (uuid) {
    this.bodyTarget.querySelector(`[data-uuid="${uuid}"]`).remove()
  }

  onDragEnter (event) {
    const target = event.target.closest('td')
    target.classList.add(bgColor)
  }

  onDragLeave (event) {
    const target = event.target.closest('td')
    target.classList.remove(bgColor)
  }

  onDragOver (event) {
    if (event.preventDefault) {
      event.preventDefault()
    }
    return false
  }

  onDragStart (event) {
    event.target.style.opacity = '0.4'
    event.dataTransfer.effectAllowed = 'move'
    event.dataTransfer.setData('text/plain', event.target.dataset.uuid)
  }

  onDragEnd (event) {
    event.target.style.opacity = '1'
  }

  onDrop (event) {
    event.preventDefault()
    event.stopPropagation()
    const target = event.target.closest('td')
    target.classList.remove(bgColor)
    this.requestMove({
      day: target.dataset.day,
      member: target.dataset.member,
      uuid: event.dataTransfer.getData('text/plain')
    })
  }

  /**
   * @param {string} uuid
   * @param {string} day
   * @param {string} member
   */
  requestMove ({ uuid, day, member }) {
    const data = new FormData()
    data.append('day', day)
    data.append('member', member)
    data.append('uuid', uuid)
    const options = {
      body: data,
      method: 'POST'
    }
    fetch('/move', options)
      .catch(error => {
        alert(`Unable to move occupation: ${error}`)
      })
  }
}
