import 'bootstrap/dist/css/bootstrap.min.css'
import './styles/app.scss'

import './bootstrap'

// const planning = document.getElementById('planning')
// if (planning) {
//   const eventSource = new EventSource(planning.dataset.subscription)
//   eventSource.onmessage = event => {
//     const payload = JSON.parse(event.data)
//     if (payload.operation === 'ADD') {
//       occupationAdd({ ...payload.occupation })
//     } else if (payload.operation === 'MOVE') {
//       occupationAdd({ ...payload.occupation })
//     }
//   }
//   const cells = planning.getElementsByTagName('td')
//   for (const cell of cells) {
//     cell.addEventListener('dragenter', onDragEnter)
//     cell.addEventListener('dragleave', onDragLeave)
//     cell.addEventListener('dragover', onDragOver)
//     cell.addEventListener('drop', onDrop)
//   }
//   const occupations = planning.querySelectorAll('[draggable=true]')
//   for (const occupation of occupations) {
//     occupation.addEventListener('dragstart', onDragStart)
//     occupation.addEventListener('dragend', onDragEnd)
//   }
// }
//
// /**
//  * @param {string} day
//  * @param {number} hour
//  * @param {string} title
//  */
// function occupationAdd ({ day, hour, title }) {
//   const element = document.createElement('div')
//   element.classList.add('bg-dander')
//   element.draggable = true
//   element.innerHTML = title
//   const parent = document.querySelector('[data-day="' + day + '"][data-hour="' + hour + '"]')
//   parent.appendChild(element)
// }
//
// function onDragEnter () {
//   this.classList.add('bg-danger')
// }
//
// function onDragLeave () {
//   this.classList.remove('bg-danger')
// }
//
// function onDragOver (event) {
//   if (event.preventDefault) {
//     event.preventDefault()
//   }
//   return false
// }
//
// function onDragStart (event) {
//   this.style.opacity = 0.4
//   event.dataTransfer.effectAllowed = 'move'
//   event.dataTransfer.setData('text/plain', event.target.dataset.id)
// }
//
// function onDragEnd () {
//   this.style.opacity = 1
// }
//
// function onDrop (event) {
//   event.preventDefault()
//   event.stopPropagation()
//   this.classList.remove('bg-danger')
//   const id = event.dataTransfer.getData('text/plain')
//   const data = new FormData()
//   data.append('day', this.dataset.day)
//   data.append('hour', this.dataset.hour)
//   data.append('occupation', id)
//   const options = {
//     body: data,
//     method: 'POST'
//   }
//   fetch('/move', options)
//     .then(response => {
//       if (response.ok) {
//         const source = document.querySelector(`[data-id="${id}"]`)
//         source.remove()
//       } else {
//         throw new Error(`Status: ${response.statusCode}`)
//       }
//     })
//     .catch(error => {
//       alert(error)
//     })
// }
