<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\MemberRepository;
use App\Repository\OccupationRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

final class MoveController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly HubInterface $hub,
        private readonly MemberRepository $memberRepository,
        private readonly OccupationRepository $occupationRepository,
    ) {
    }

    /**
     * @throws NonUniqueResultException
     * @throws JsonException
     */
    #[Route('/move', name: 'app_move', methods: ['POST'])]
    public function __invoke(Request $request): Response
    {
        $day = (string) $request->request->get('day');
        $day = DateTimeImmutable::createFromFormat('Y-m-d', $day);
        if (!$day) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }
        $member = (string) $request->request->get('member');
        $member = $this->memberRepository->getByUuid(Uuid::fromString($member));
        $occupation = (string) $request->request->get('uuid');
        $occupation = $this->occupationRepository->getByUuid(Uuid::fromString($occupation));
        $occupation->moveTo($day, $member);
        $this->entityManager->flush();

        $update = new Update(
            'occupations',
            json_encode([
                'operation' => 'MOVE',
                'occupation' => $occupation,
            ], JSON_THROW_ON_ERROR)
        );
        $this->hub->publish($update);

        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
