<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Occupation;
use App\Form\OccupationData;
use App\Form\OccupationType;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

final class AddController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly HubInterface $hub,
    ) {
    }

    /**
     * @throws JsonException
     */
    #[Route('/add', name: 'app_add', methods: ['GET', 'POST'])]
    public function __invoke(Request $request): Response
    {
        $data = new OccupationData();
        $form = $this->createForm(OccupationType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $occupation = new Occupation(Uuid::v4(), $data->member, $data->day, $data->title);
            $this->entityManager->persist($occupation);
            $this->entityManager->flush();

            $update = new Update(
                'occupations',
                json_encode([
                    'operation' => 'ADD',
                    'occupation' => $occupation,
                ], JSON_THROW_ON_ERROR)
            );
            $this->hub->publish($update);

            $this->addFlash('success', 'Occupation added.');

            return $this->redirectToRoute('app_add');
        }

        return $this->render('add.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
