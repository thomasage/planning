<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\MemberRepository;
use App\Repository\OccupationRepository;
use DateInterval;
use DatePeriod;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class DefaultController extends AbstractController
{
    public function __construct(
        private readonly MemberRepository $memberRepository,
        private readonly OccupationRepository $occupationRepository,
    ) {
    }

    #[Route('/', name: 'app_index')]
    public function __invoke(): Response
    {
        /** @var DateTimeImmutable $start */
        $start = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', date('Y-m-01 00:00:00'));
        /** @var DateTimeImmutable $end */
        $end = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', date('Y-m-t 23:59:59'));
        $days = new DatePeriod($start, new DateInterval('P1D'), $end);

        $members = $this->memberRepository->findBy([], ['firstName' => 'ASC']);
        $occupations = $this->occupationRepository->findBetween($start, $end);

        return $this->render('index.html.twig', [
            'days' => $days,
            'members' => $members,
            'occupations' => $occupations,
        ]);
    }
}
