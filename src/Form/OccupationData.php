<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Member;
use DateTimeImmutable;

final class OccupationData
{
    public DateTimeImmutable $day;
    public Member $member;
    public string $title;

    public function __construct()
    {
        $this->day = new DateTimeImmutable();
    }
}
