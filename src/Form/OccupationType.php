<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Member;
use App\Repository\MemberRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class OccupationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'day',
                DateType::class,
                [
                    'input' => 'datetime_immutable',
                    'widget' => 'single_text',
                ]
            )
            ->add(
                'member',
                EntityType::class,
                [
                    'class' => Member::class,
                    'query_builder' => static function (MemberRepository $memberRepository): QueryBuilder {
                        return $memberRepository->createQueryBuilder('m')->addOrderBy('m.firstName');
                    },
                ]
            )
            ->add(
                'title',
                TextType::class
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => OccupationData::class,
        ]);
    }
}
