<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Occupation;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

/**
 * @extends ServiceEntityRepository<Occupation>
 */
final class OccupationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Occupation::class);
    }

    /**
     * @return Occupation[]
     */
    public function findBetween(DateTimeImmutable $start, DateTimeImmutable $end): array
    {
        $builder = $this
            ->createQueryBuilder('occupation')
            ->andWhere('occupation.day BETWEEN :start AND :end')
            ->setParameter('end', $end)
            ->setParameter('start', $start);

        return $builder->getQuery()->getResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getByUuid(Uuid $uuid): Occupation
    {
        return $this
            ->createQueryBuilder('occupation')
            ->andWhere('occupation.uuid = :uuid')
            ->setParameter('uuid', $uuid)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
