<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Member;
use App\Entity\Occupation;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Uid\Uuid;

final class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $memberFox = new Member(Uuid::v4(), 'Fox');
        $manager->persist($memberFox);

        $memberDana = new Member(Uuid::v4(), 'Dana');
        $manager->persist($memberDana);

        $memberWalter = new Member(Uuid::v4(), 'Walter');
        $manager->persist($memberWalter);

        $occupation = new Occupation(Uuid::v4(), $memberFox, new DateTimeImmutable('2022-01-10'), 'Lorem ipsum');
        $manager->persist($occupation);

        $occupation = new Occupation(Uuid::v4(), $memberFox, new DateTimeImmutable('2022-01-10'), 'I want to believe');
        $manager->persist($occupation);

        $occupation = new Occupation(Uuid::v4(), $memberFox, new DateTimeImmutable('2022-01-10'), 'Something else');
        $manager->persist($occupation);

        $manager->flush();
    }
}
