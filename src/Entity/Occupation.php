<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\OccupationRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: OccupationRepository::class)]
class Occupation implements JsonSerializable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer', options: ['unsigned' => true])]
    private ?int $id;

    #[ORM\Column(type: 'uuid')]
    private Uuid $uuid;

    #[ORM\ManyToOne(targetEntity: Member::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Member $member;

    #[ORM\Column(type: 'date_immutable')]
    private DateTimeImmutable $day;

    #[ORM\Column(type: 'string', length: 100)]
    private string $title;

    public function __construct(Uuid $uuid, Member $member, DateTimeImmutable $day, string $title)
    {
        $this->day = $day;
        $this->member = $member;
        $this->title = $title;
        $this->uuid = $uuid;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getDay(): DateTimeImmutable
    {
        return $this->day;
    }

    public function setDay(DateTimeImmutable $day): self
    {
        $this->day = $day;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getMember(): Member
    {
        return $this->member;
    }

    public function moveTo(DateTimeImmutable $day, Member $member): void
    {
        $this->day = $day;
        $this->member = $member;
    }

    /**
     * @return array{day:string,title:string,uuid:string}
     */
    public function jsonSerialize(): array
    {
        return [
            'day' => $this->day->format('Y-m-d'),
            'member' => (string) $this->member->getUuid(),
            'title' => $this->title,
            'uuid' => (string) $this->uuid,
        ];
    }
}
