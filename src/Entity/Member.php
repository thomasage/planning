<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\MemberRepository;
use Doctrine\ORM\Mapping as ORM;
use Stringable;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: MemberRepository::class)]
class Member implements Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer', options: ['unsigned' => true])]
    private ?int $id;

    #[ORM\Column(type: 'uuid', unique: true)]
    private Uuid $uuid;

    #[ORM\Column(type: 'string', length: 50)]
    private string $firstName;

    public function __construct(Uuid $uuid, string $firstName)
    {
        $this->firstName = $firstName;
        $this->uuid = $uuid;
    }

    public function __toString(): string
    {
        return $this->firstName;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }
}
